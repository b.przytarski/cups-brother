#!/bin/bash

/usr/sbin/cupsd
while [ ! -f /var/run/cups/cupsd.pid ]; do sleep 1; done
cupsctl --remote-admin --remote-any --share-printers
printf 'y\ny\ny\nn\nn\n'| bash /root/$BROTHER_INSTALLER $PRINTER_MODEL

lpadmin -p $PRINTER_MODEL -v socket://$PRINTER_IP
sed -i "s/HL1210W/${PRINTER_MODEL}/g" /etc/avahi/services/AirPrint-HL1210W.service
mv /etc/avahi/services/AirPrint-HL1210W.service /etc/avahi/services/AirPrint-${PRINTER_MODEL}.service

exec /usr/sbin/avahi-daemon