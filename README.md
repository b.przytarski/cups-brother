# cups-brother

Docker image with included:

* cups
* brother drivers
* avahi-daemon - for airprint
* adds network printer to cups

## usage

docker-compose:

Use environment varables to specify printer type and printer ip

```yaml
  cupsd:
    container_name: cupsd
    image: registry.gitlab.com/b.przytarski/cups-brother
    dns: 192.168.10.1
    environment:
      - PRINTER_MODEL=HL1210W
      - PRINTER_IP=192.168.10.55
    volumes:
      - /etc/localtime:/etc/localtime:ro
    restart: always
    networks:
      00_qnet-static-eth0:
        ipv4_address: 192.168.10.15
      02_internal_network:
```
