FROM debian:buster

ENV PRINTER_MODEL=HL1210W
ENV PRINTER_IP=192.168.10.55
ENV BROTHER_INSTALLER=linux-brprinter-installer-2.2.2-1

RUN apt-get update && apt-get -y install \
cups \
cups-filters \
cups-pdf \
whois \
lpr \
usbutils \
lib32stdc++6 \
lib32gcc1 \
libc6-i386 \
libusb-0.1-4 \
wget \
sudo \
&& rm -rf /var/lib/apt/lists/*

# Add user and disable sudo password checking
RUN useradd \
  --groups=sudo,lp,lpadmin \
  --create-home \
  --home-dir=/home/print \
  --shell=/bin/bash \
  --password=$(mkpasswd print) \
  print \
&& sed -i '/%sudo[[:space:]]/ s/ALL[[:space:]]*$/NOPASSWD:ALL/' /etc/sudoers

COPY avahi/avahi-daemon.conf /etc/avahi/
COPY avahi/*.service /etc/avahi/services/
COPY $BROTHER_INSTALLER /root
COPY ./docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
EXPOSE 631

HEALTHCHECK CMD ss -an | grep 631 > /dev/null; if [ 0 != $? ]; then exit 1; fi;

ENTRYPOINT ["/docker-entrypoint.sh"]